<?php 
     include 'database.php';
     session_start();

     $baza = "select * from Questions";
     $tabela = $mysqli->query($baza) or die($mysqli->error.__LINE__);
     $ile = $tabela->num_rows;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Quiz</title>
</head>

<body>
    <h1>Quiz</h1>
    <b>Gratulacje!</b>
    <br>
    I completed test
    <br>
    You have scored<b><?php echo $_SESSION['score']; ?>/<?php echo $ile;?></b>
    <br>
    <input type="button" value="Back Start Page" onClick="location.href='main.php';">
</body>
</html>

<?php session_destroy(); ?>
