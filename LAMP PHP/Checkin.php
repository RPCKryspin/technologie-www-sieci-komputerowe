<?php 
     include 'database.php';
     session_start();

     $baza = "select * from Questions";
     $tabela = $mysqli->query($baza) or die($mysqli->error.__LINE__);
     $ile = $tabela->num_rows;

    if(!isset($_SESSION['score']))
    {
        $_SESSION['score'] = 0;
    }

    if($_POST)
    {
        $numer = $_POST['id'];
        $poprawna = $_POST['Ans'];

        if($poprawna == 1)
            $_SESSION['score']++;

        if($numer == $ile)
        {
            header("Location: Complete.php");
            exit();
        }
        header("Location: Quest.php?n=".($numer+1));
    }
?>
